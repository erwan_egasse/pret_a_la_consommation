package exceptions;

public class DureeExcessiveException extends Exception
{
    public DureeExcessiveException(String s) { super(s); }
}

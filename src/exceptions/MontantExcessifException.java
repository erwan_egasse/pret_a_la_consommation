package exceptions;

public class MontantExcessifException extends Exception
{
    public MontantExcessifException(String s) { super(s); }
}
